/* Escuela Politécnica Nacional 
 * Proyecto Final de Sistemas Operativos
 * MiniShell 
 * Autor: José Luis Escobar Cárdenas
 * Facultad: Ingenieria de Sistemas*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h> //Manejo Archivos
#include <unistd.h> 
#include <sys/types.h>
#include <sys/stat.h> //Estado de un archivo
#include <sys/wait.h> //manejo de wait
#include <sys/mman.h> //para gestion de memoria
#include <sys/times.h> //Gestion de E/S Contabilidad 
#include <sys/stat.h> //Para información de un archivo
#include <time.h> //Manajo de Fecha y Hora
#include <semaphore.h> //Semáforos POSIX
#include <pthread.h> //Hilos POSIX
#include <signal.h> //Manejo de Señales 

#define PROMPT "Jose Luix"
#define FALSE 0
#define TRUE 1

//Estructura que contiene el nombre del comando y sus argumentos
typedef struct command_t {
  char *nombre;
  int argc;
  char **argv;
} TipoComando;
//Funciones prototipo a usar en el programa 
char * ConseguirRutaComando(char* command);
int CorrerComando(TipoComando* command);
int MenuGestion();
//Funciones sobre gestion del Sistema Operativo
int  GestionProcesos();
int  GestionThreads();
int  GestionSeniales();
int  PlanProcesador();
int  GestionMemoria();
int  GestionComunic();
int  GestionSync();
int  GestionES();
int  GestionArchivos();
//Funciones Auxiliares
void *imprimir(void *); //GestionThreads();
void trataralarma(); //GestionSeniales();
void funcion(char *); //PlanProcesador();
void manejador(void *); //GestionSync();
void funcionES(); //GestionES();
//Variables Globales
char *path; //Variable donde se alamacenará el path
char *ptr;   // Variable auxiliar
char *input;  //Variable auxiliar para manejo de archivos
int pid;    //Variable donde se almacenará el Pid
int status;   //Variable donde se almacenará el status del proceso 
int contador; /*Variable compartida por 2 diferentes hilos de GestionSync()*/
sem_t semaforo; //Semáforo a ser usado por GestionSync()

char* ConseguirRutaComando(char* command) {

  FILE *fpin;   //Estructura File Pointer to Input
  char p[1024];  
  char *temppath;
  char *temp = (char *)calloc(1024, sizeof(char));

  path = (char *) getenv("PATH"); 

  strcpy ( p, path );
  path = strtok ( p, ":" );   // Encuentra el primer PATH
  while ( path != NULL ) {
    strcpy ( temp, path );  // Guarda una copia del substring
    strcat ( temp, "/" );     // forma el path
    strcat ( temp, command );
    if ( ( fpin = fopen ( temp, "r" ) ) == NULL ) {
      temp[0] = '\0';  // Comienza de nuevo
      path = strtok ( ( char * ) 0, ":" );  // prueba el siguiente path
    } else {
      break; 
    }
  }
  if ( fpin == NULL ) {
    return NULL;
  } else {
    return temp;
  }

}


/*Función Main
 * No recibe  argumentos como el contador argc ni el 
 * arreglo argv ya que se los ha definido anteriormente
 * en la estructura TipoComando*/
int main() {
  //Reservamos memoria dinámica para la estructura que recibirá 
  //los comandos 
  TipoComando *comando = (TipoComando*) malloc(sizeof(TipoComando)); 
  char *opcion=(char*)calloc(2,sizeof(char));
  char* buffer = (char*)calloc(1024, sizeof(char));
  //Imprimimos el Menú Inicial
  while(TRUE)
  { 
  printf("\n\n\t\t\tMiniShell JoseLuix\n\n");
  printf("Menú\n\n");
  printf("1.-Ejecutar Comandos Internos\n");
  printf("2.-Abrir Menú de Gestión de Sistema Operativo\n");
  printf("3.-Salir de la MiniShell\n");
  gets(opcion);
 
  switch(atoi(opcion))
  {
    case 1:
    printf("Digite back para regresar al menú inicial\n");
    while(TRUE)
    {
     //Almacenamos memoria dinámica para la entrada por consola    
    input = (char *)calloc(1024, sizeof(char));
    /* imprime el prompt
     getcwd(Get Current Working Directory): Nos muestra
     el directorio actual sobre el que estamos trabajando*/
    printf("%s - (%s):> ", PROMPT, getcwd(buffer, 1024)); 
    gets(input);
    // vuelve a pedir los datos si se ingresa una linea en blanco, un punto o dos puntos
    if ((strcmp(input, "") != 0) && (strcmp(input, ".") != 0) && (strcmp(input, "..") != 0)) {
      //Verifica si se ha ingresado quit o exit para salir de la shell
      if ((strcmp(input, "quit")== 0) || (strcmp(input, "exit")== 0)) 
      {
        printf("Saliendo de la Shell...\n");
        return 0;
      } //EndIf
      //Verifica si se ha intresado back para regresar al menu inicial
      if((strcmp(input,"back")==0||strcmp(input,"BACK")==0))
      {
	  break; //Regresa al Menú
      }
  
      // inicializa el arreglo argv
      comando->argv = (char**)calloc(256, sizeof(char*)); 
       
     
      // divide en tokens al comando ingresado
      // y almacena cada argumento en el arreglo argv
      ptr = strtok(input, " ");
      comando->nombre = ptr;
      comando->argv[0] = ptr;
      comando->argc = 1;
      int count = 1;
      ptr = strtok (NULL, " ");
        while (ptr != NULL)
        {
          
              comando->argv[count++] = ptr;
              comando->argc++;
               ptr = strtok (NULL, " ");
        }
           CorrerComando(comando);
	}//EndIf
      }
	break; //EndWhile
      case 2:
       if(MenuGestion()==TRUE)
       break;
      case 3:
       printf("Saliendo de la Shell...\n");
       return 0;
      default:
        break;
     }//EndSwitch
   }//EndWhile
}//EndMain




// Funcion para ejecutar el comando
int CorrerComando(TipoComando* comando) {
  /*Invocamos a la función CoseguiRutaComando
 * para obtener la ruta de la carpeta del comando
 * ingresado*/
  char* Ruta = ConseguirRutaComando(comando->nombre);
  if (Ruta == NULL) {
    printf("Comando no Encontrado\n");
    return 1;
  }

  // Crea un proceso Hijo
  pid = fork();
  // Verifica si existe algun error
  if (pid < 0) {
    printf("Error en la creacion del proceso\n");
    return 1;
  } else if (pid == 0) {
    // Codigo del hijo
    execv(Ruta, comando->argv);
    return 1;
  } else {
    // El padre (shell) espera
    while (wait(&status) != pid);
  }
}

/*Funcion desde donde controlaremos los diferentes programas
 * a ejecutarse sobre Gestión del Sistema Operativo*/
int MenuGestion()
{
   char *opcion=(char*)calloc(5,sizeof(char));
   //Iniciamos el Nuevo Menu
   while(TRUE)
   {
      printf("\t\t\tMenú de Gestión del Sistema Operativo\n\n\n");
      printf("1.- Gestión de Procesos\n\n");
      printf("2.- Gestión de Procesos Ligeros(Threads)\n\n");
      printf("3.- Gestión de Señales\n\n");
      printf("4.- Planificiación del Procesador\n\n");
      printf("5.- Gestión de Memoria\n\n");
      printf("6.- Comunicación\n\n"); 
      printf("7.- Sincronización\n\n");
      printf("8.- Gestion de Entrada y Salida\n\n");
      printf("9.- Gestion de Archivos\n\n");
      printf("10.- Regresar al Menú Principal\n");
      gets(opcion);
      
      switch(atoi(opcion))
      {
         case 1:
	    if(GestionProcesos()==TRUE)
	    break;
	 case 2:
	    if(GestionThreads()==TRUE)
            break;
	 case 3:
	    if(GestionSeniales()==TRUE)
 	    break;
	 case 4:
	    if(PlanProcesador()==TRUE)
	    break;
	 case 5:
	    if(GestionMemoria()==TRUE)
	    break;
	 case 6:
	    if(GestionComunic()==TRUE)
	    break;
	 case 7:
	    if(GestionSync()==TRUE)
	    break;
	 case 8:
	    if(GestionES()==TRUE);
	    break;
	 case 9:
	    if(GestionArchivos()==TRUE);
	    break;
	 case 10:
	 return 1;
 	 default:
         break;
       } //End Switch
   }//EndWhile
}//FinFuncion
          
/*Inicio de la Función que Ejecutará el programa 
 * de gestión de procesos; este programa imprimirá 
 * la información de idenfiación del proceso acutal
 * en este caso la MiniShell*/

int GestionProcesos()  
{  
   char *opcion= (char*)calloc(2,sizeof(char));
   printf("\t\t\t***Descripción del Programa***\n\n");
   printf("Este programa Imprime la información de identifiación de un proceso.\n");
   printf("Información Que se Desplegará:\n");
   printf("- ProcessID\n");
   printf("- Identificador de Usuario\n");
   printf("- Identificador de Usuario Efectivo\n");
   printf("- Identificador de Grupo\n");
   printf("- Identificador de Grupo Efectivo\n\n");
   printf("¿Desea Ejecutar El Programa? S/N\n");
   gets(opcion);
   //Si no se desea ejecutar el programa se regresa el menu anterior
   if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
   {
	return 1;
   }
   else
   {
	printf("Identificador del Proceso Pid: %d\n",getpid());
   	printf("Identificador de Usuario: %d\n",getuid());
 	printf("Identificador de Usuario Efectivo: %d\n",geteuid());
 	printf("Identificador de Grupo: %d\n",getgid());
	printf("Identificador de Grupo Efectivo: %d\n",getegid());
	printf("Presione Una Tecla Para Continuar...\n");
	gets(opcion);
        return 1;
   }
}

/*Función que creará 1 therad para llevar a cabo
 * la ejecución de una función, luego de ejecutarla el hilo
 * padre main() esperará a la finalización de el thread 
 * y posteriormente se realizará un join para volver a finalizar
 * "juntar" el hilo padre con el hilo creado*/
int GestionThreads()
{ 
   char* opcion=(char*)calloc(2,sizeof(char));
   printf("\t\t\t***Descripcion del Programa***\n\n");
   printf("Este programa crea un thread que ejecuta una funcion llamada imprimir()\n");
   printf("esta función imprimirá  un mensaje por 5 veces, el Hilo Padre main() esperará\n");
   printf("a su finalización y el hilo terminará su ejcución\n");  
   printf("¿Desea Ejecutar El Programa? S/N\n");
   gets(opcion);
   //Si no se desea ejecutar el programa se regresa al menú anterior
   if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
   {
	return 1;
   }
   else
   {
	pthread_t thread1; //creamos un identificador de Hilo
        /*Creamos el Hilo y enviamos la funcion a ejecutar*/
 	if(pthread_create(&thread1,NULL,imprimir,NULL)<0)
 	{
	   printf("Error al crear el hilo");
	   exit(-1);
	}
	/*Una vez concluida la ejecución terminamos el hilo juntandolo
 *  	de nuevo con el hilo padre*/
	if(pthread_join(thread1,NULL)<0)
 	{
	  printf("Error al ejecutar el servicio pthread_join");
	  exit(-1);
	}
   }
	printf("Presione una tecla para continuar...");
	gets(opcion);
	return 1;	  
}

/*Funcion auxiliar para uso exclusivo de la función 
 * GestionThreads()*/

void* imprimir(void * argumento)
{
     int i; 
     for(i=1;i<6;i++)
     {  /*pthread nos devuelve el identificador de hilo*/
	printf("Soy el Hilo %lu y te digo Hola la %d Vez\n",pthread_self(),i);
       
     }
     pthread_exit(0);
} 

/* Función para la gestión de señales
 * se realizará un programa que imprime un mensaje
 * cada 3 segundos mediante un temporizador y luego se 
 * suspende la ejecucion hasta que recibe la señal 
 * SIGALRM la cual es señal de fin de temporizador*/
int GestionSeniales()
{   
    char* opcion= (char*)calloc(2,sizeof(char));

    printf("\t\t\t***Descripcion del Programa***\n\n\n");
    printf("Este programa ejecutará una función cada 3 segundos para ello arma \n"); 
    printf("un manejador para la señal SIGALRM mediante el servicio sigaction\n"); 
    printf("Esta funcion Imprimirá la linea -activada- cada vez que se envie \n");
    printf("la señal SIGALRM\n");
    printf("¿Desea Ejecutar el Programa? S/N\n");
    gets(opcion);
    if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
    {
	return 1;
    }
    else
    {
    struct sigaction act; //estructura de manejo de señales
  

   /*Establece el manejador*/
   act.sa_handler=trataralarma; //invocamos al miembro de la estructura sigaction
				//para que ejecute la funcion tratar_alarma cuando
				//reciba la señal
 
   act.sa_flags=0;   //Ninguna Accion especial a realizar
   
   /*Se bloquea la recepción de la señal SIGINT (Ctrl+C) cuando se ejecute
 * la función tratar_alarma*/
   
    sigemptyset(&act.sa_mask); //iniciamos un conjunto de señales
    sigaddset(&act.sa_mask,SIGINT); //Agregamos la señal SIGINT al conjunto 

    sigaction(SIGALRM,&act,NULL);
    int i=0;    
    for(i=0;i<5;i++)
    {   printf("Enviando Señal...");
	alarm(3); //Se establece el temporizador a 3 segundos
	pause();  /*Se suspende el proceso hasta que se reciba 
		  la señal SIGALRM*/	
    }
    
    }
   printf("Presione una tecla para continuar...");
   gets(opcion);
   return 1;
}   
//Función auxiliar para uso exclusivo de la función 
//GestionSeniales() imprime la linea  "activada" */

void trataralarma()
{
   printf("Activada\n");
}
	
/*Función para Gestión del Procesador, se cambiará la 
 * prioridad de un proceso mediante el servicio nice()
 * Se creará un proceso padre y un hijo y ambos ejecutaran
 * una funcion para escribir un numero elevado de mensajes 
 * de acuerdo al cambio de prioridad se podrá ver como su 
 * tiempo de ejecución varía*/
int PlanProcesador()   	
{  
    char *opcion=(char*)calloc(2,sizeof(char)); 
    char *prioridad= (char*)calloc(5,sizeof(char));
    printf("\t\t\t***Descripción del Programa***\n\n\n");
    printf("Este programa permite ver el efecto de modificar la prioridad\n");
    printf("de un proceso. Se creará un proceso hijo y se pedirá al usuario\n");
    printf("ingresar un valor para la prioridad. Si no se realizara esta modificación\n");
    printf("o si se le asigna el valor de 0 los dos procesos terminarian su\n");
    printf("ejecución en tiempos aproximadamente iguales pero se podrá apreciar\n");
    printf("que para valores >0 el padre terminará antes que el proceso hijo\n");
    printf("La función a ejecutar será la impresión de un mensaje 20000 veces\n");
    printf("¿Desea Ejecutar el programa S/N?\n");
    gets(opcion);
    if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
    {
	return 1;
    }
    else
    {	
    printf("Ingrese la prioridad para el proceso hijo\n");
    printf("Recuerde!: Mayor Valor < Prioridad\n");
    gets(prioridad);
    int prio_hijo;
    /*Variable para establecer la prioridad del hijo
     entre mayor es el valor menor es la prioridad
    valores menores a 0 solo son permitidos para superusuarios*/
    if((prio_hijo=atoi(prioridad))==0)
    {
	perror("Prioridad no válida");
	return 1;
    }
    	
    int pid=fork();
    /*Creamos un Proceso Hijo*/
    if(pid==0)
    {
	/*Cambiamos la prioridad del proceso hijo*/
	if(nice(prio_hijo)<0)
	{
	   perror("Error cambiando prioridad");
	   exit(-1);
	}
	funcion("hijo");//Ejecutamos la funcion para el proceso hijo
    	exit(1); //Termina la ejecucion del hijo
    }
    else
    {
      	funcion("padre"); //Ejecuta la funcion el padre
        while(wait(&status)!=pid); //El padre (Shell) espera la terminacion del hijo
    }  
    printf("Presione una tecla para continuar...");
    gets(opcion);
    return 1;
    	

   }//endelse
}


/*Función Auxiliar que realiza la impresión del mensaje 
 * un millon de veces*/

void funcion(char * proceso)
{
     int i;
     static int const  iteraciones=20000;
     for(i=0;i<iteraciones;i++)
     {
	printf("proceso %s iteracion %d\n",proceso,i);
        
     }
}

/*Funcion para gestión de memoria. Este programa permite la proyeccion
 * de un archivo en memoria para facilitar y optimizar su acceso 
 * una vez abierto se contará el numero de veces que un caracter especificado
 * por el usuario se repite. Al Finalizar la operación se desproyecta el archivo
 * de memoria*/
int GestionMemoria()
{
   char * opcion= (char*)calloc(4,sizeof(char));
    
   printf("\t\t\t***Descripción del Programa***\n\n\n");
   printf("Este programa realizará la proyección de un archivo en memoria de esta manera\n");
   printf("se optimizará su acceso, Se ejecutará una función que contará el numero de veces\n");
   printf("que un caracter especificado por el usuario se repite.Al finalizar esa opreación\n");
   printf("se desproyecta el archivo de memoria\n");
   printf("¿Desea ejecutar el programa? S/N");
   gets(opcion);
   if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
   {
	return 1;
   }
   else
   {  
	char* caracter=(char*)calloc(1,sizeof(char)); //Variable que recibirá el caracter
	char* nombrearchivo=(char*)calloc(100,sizeof(char));
        printf("Ingrese el caracter que desea buscar\n");
        gets(caracter);
	printf("Ingrese el nombre del archivo que desea abrir\n");
        gets(nombrearchivo);
        int i,fd,contador=0;  //definimos las variables i, el descriptor de archivo y el contador 
        struct stat statusarchivo; //estrucutra con la cual averiguaremos informacion del i-node del archivo
        char *org, *p; //variables donde almacenaremos los valores devueltos por las funciones mmap y munmap
	//Abrimos el archivo para lectura
        if((fd=open(nombrearchivo,O_RDONLY))<0)
  	{
	    perror("No se ha encontrado el archivo especificado\n");
	    return 1;
  	}
	/*Averiguamos la longitud del archivo obteniendo información de su 
 *      i-node*/
        if((fstat(fd,&statusarchivo)<0))
        {
	    perror("Error al intentar acceder al i-node del archivo\n");
	    close(fd);//cerramos el archivo
	    return 1;
	}
	/*Se proyecta el archivo*/
        /*Los parametros recibidos por la funcion son en este orden:
 *  	direccion donde se quiere proyectar, tamaño del archivo, proteccion de la region
 *  	propiedad de la region en este caso compartida, el descriptor de archivo y el 
 *  	desplazamiento dentro del archivo en este caso 0*/
        if((org=mmap((caddr_t) 0,statusarchivo.st_size,PROT_READ,MAP_SHARED,fd,0))==MAP_FAILED)
        {
	    perror("Error en la proyección del archivo\n");
	    close(fd);
	    return 1;
	}
	/*Se cierra el archivo*/
	close(fd);
	/*Bucle de acceso al archivo*/
	p=org; //copiamos la direccion de memoria 
	for(i=0;i<statusarchivo.st_size;i++)
	{
	    if(*p++==*caracter) contador++; //si encuentra el caracter que aunmente el contador en 1
	}
	/*Desproyectamos el archivo*/
	munmap(org,statusarchivo.st_size);
	printf("El Caracter %s se repite %d veces\n",caracter,contador);
	printf("Presione una tecla para continuar...");
	gets(opcion);
	return 1;
	}//EndElse
}

/*Función para Comunicacion. Esta función permitirá el 
 * paso de mensajes entre un proceso padre y un proceso hijo
 * usando tuberías. El padre enviará un mensaje al hijo que desplegará 
 * en pantalla*/

int GestionComunic()
{   	
	char *opcion=(char*)calloc(2,sizeof(char));
	printf("\t\t\t***Descripción del Programa***\n\n\n");
	printf("Este programa mostrará la comunicación entre 2 procesos (Padre-Hijo)\n");
	printf("el padre enviará un mensaje al proceso hijo el cuál se desplegará en\n");
	printf("pantalla\n");
	printf("¿Desea ejecutar el programa S/N?\n");
	gets(opcion);
	if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
	{
	   return 1;
	}
	else
	{
	int filedes[2];//Descriptores para la tubería
	int bytesleidos;
	int bytesescritos;
	char *mensajeenv="Hola Hijo!"; //Mensaje a enviar
        char mensajerecibido[100]; //Donde recibiremos el msj
	/*Iniciamos el pipe*/
 	pipe(filedes);
	/*Creamos el proceso hijo*/
	int pid=fork();
	if(pid==0)
        {
	     close(filedes[1]);//cerramos el descriptor de escritura
	     bytesleidos= read(filedes[0],mensajerecibido,100); //Leemos la tubería
	     mensajerecibido[bytesleidos]='\0'; //Agregamos el fin de cadena 
	     printf("El mensaje enviado por mi padre es %s y tiene %d bytes\n",mensajerecibido,bytesleidos);
	     close(filedes[0]); //cerramos el descriptor de lectura
	     exit(1);  //Terminamos la ejecución del proceso hijo
	}
	else
	{
	     printf("Soy el padre y voy a enviar un mensaje a mi hijo\n");
	     close(filedes[0]); //cerramos el descriptor de lectura
	     bytesescritos=write(filedes[1],mensajeenv,strlen(mensajeenv));//escribimos en la tuberia
	     printf("Se han escrito %d bytes \n",bytesescritos);
 	     close(filedes[1]); //cerramos el descriptor de escritura
	     while(wait(&status)!=pid); //la shell espera a que termine el hijo
	}
	printf("Presione una tecla para continuar...\n");
	gets(opcion);
	return 1;
        }//endelse
}

/*Función para la Sincronización de Procesos e Hilos. Este programa
 * creará 2 hilos que ejecutarán una función que incrementará el valor
 * de un contador que será una variable compartida entre los hilos 
 * se usarán semáforos para garantizar la sección crítica entre los 
 * hilos*/

int GestionSync()
{
	char *opcion=(char*)calloc(2,sizeof(char));
	
	printf("\t\t\t***Descripción del Programa***\n\n\n");
	printf("El siguiente programa creará 2 hilos que ejeuctarán una función que\n");
	printf("incrementará el valor de un contador que será una variable compartida\n");
	printf("entre los hilos.Se usarán semáforos para asegurar la sección crítica\n");
	printf("con respecto al acceso de los hilos a dicha variable\n");
	printf("¿Desea ejecutar el programa? S/N\n");
	gets(opcion);
	if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
	{
	   return 1;
	}
	else
	{
	   int i[2]; //argumento para la funcion manejador
	   
	   //identificadores de hilos
	   pthread_t hiloA;
	   pthread_t hiloB;
	 
	   /*Inicializamos el arreglo i*/
	   i[0]=0;
	   i[1]=1;
	 
           /*Inicializacmos el semaforo
	   Argumentos del servicio:
 * 	   en este orden: &semaforo: puntero al semaforo global
 * 	                  0: Semaforo para sincronizar hilos 
 * 	                  1: Valor inicial del semáforo->binario
 * 	                  */ 
	   sem_init(&semaforo,0,1);
	   
	   /*Creamos los hilos A y B*/
	   pthread_create(&hiloA,NULL,(void *) manejador,(void *) &i[0]);
	   pthread_create(&hiloB,NULL,(void * )manejador,(void *) &i[1]);
            
	   /*Juntamos los hilos con el hilo principal*/
	   pthread_join(hiloA,NULL);
	   pthread_join(hiloB,NULL);
	 
	   /*Destruimos el Semáforo*/
	   sem_destroy(&semaforo);
	   printf("Presione una tecla para continuar...");
	   gets(opcion);
	   return 1;
        }//Endelse
}

void manejador(void * ptr)
{
     int x; //Numero de hilo entrando a la función
     x= *((int*)ptr); //dereferenciamos el parámetro ptr
     printf("Hilo %d: Esperando a entrar en sección crítica...\n",x);
     sem_wait(&semaforo); //Realizamos una opreacion wait sobre el semaforo
     /*Inicio de la Sección Crítica*/
     printf("Hilo %d: Ahora en Sección Crítica...\n");
     printf("Hilo %d: Valor del contador %d\n",x,contador); 
     printf("Hilo %d: Incrementando su valor...\n",x);
     contador++;
     printf("Hilo %d: Nuevo valor del contador %d\n",x,contador);
     printf("Hilo %d: Saliendo de sección crítica...\n",x);
     /*Fin de la Sección Crítica*/
     sem_post(&semaforo); //Realizamos una operación post sobre semaforo
     pthread_exit(0);     /*Salimos del hilo*/
}

/*Funcion para la gestión de Entrada y Salida. El siguiente programa 
 * mostrará el tiempo real, el tiempo en modo usuario y en modo sistema que
 * se consume durante la ejecución de un proceso */

int GestionES()
{
      char * opcion= (char*)calloc(2,sizeof(char));
      printf("\t\t\t***Descripción del Programa***\n\n\n");
      printf("Este programa mostrará el tiempo real, el tiempo en modo usuario y en modo\n");
      printf("sistema que se consume durante la ejecución de un proceso\n");
      printf("¿Desea ejecutar el programa? S/N");
      gets(opcion);
      if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
      {
	  return 1;
      }
      else
      {
	 struct tms Infoinicio, Infofin; //Estrucutras para manejo del servicio times
         clock_t t_inicio, t_fin; 
         long tickporseg;
         int pid;     
         /*Obtenemos el número de interrupciones de reloj por segundo*/
         tickporseg=sysconf(_SC_CLK_TCK);
         //Obetenemos la información del tiempo desde el inicio del proceso
         t_inicio=times(&Infoinicio); 
	 
	 /*Creamos un proceso hijo*/
         pid=fork();
	 if(pid==0)
         {
		funcionES();
	        exit(1);
         }
	 
	    wait(&status)!=pid; //Shell Espera
            //Obtenemos información del tiempo al fin de la ejeución     
            t_fin=times(&Infofin);
	    printf("Tiempo Real: %8.2f\n",(float)(t_fin-t_inicio)/tickporseg);
            printf("Tiempo de Usuario: %8.2f\n",(float)(Infofin.tms_cutime-Infoinicio.tms_cutime)/tickporseg);
            printf("Tiempo de Sistema: %8.2f\n",(float)(Infofin.tms_cstime-Infoinicio.tms_cstime)/tickporseg);
   	    printf("Presione Una Tecla Para Continuar...\n");
	    gets(opcion);
       }//Endelse
}
/*Funcion auxiliar de uso exclusivo de GestionES()
 * imprime un mensaje 1000 veces*/
void funcionES()
{
      int i=0;
      for(i=0;i<1000;i++)
      {
	 printf("Hola soy el proceso hijo saludando la %d vez\n",i);
      }
}


/*Funcion para Gestion de archivos. Este programa imprimirá 
 * la identidad del dueño, de su grupo y los derechos de acceso 
 * el tamaño del archivo, la hora de último acceso y la hora
 * de ultima modificación de  un archivo obtiendo esta información de su i-node*/

int GestionArchivos()
{
	char * opcion= (char*)calloc(2,sizeof(char));
	char * archivo= (char*)calloc(128,sizeof(char));
	printf("\t\t\t***Descripción del Programa***\n\n\n");
	printf("Este programa imprimirá la identidad del dueño, del grupo  los\n");
	printf("derechos de acceso, El tamaño del archivo,la fecha y hora  de \n"); 
	printf("ultimo acceso y la fecha y  hora de última modificación de un \n");
	printf("archivo obteniendo esta información de su i-node\n");
	printf("¿Desea Ejecutar el Programa? S/N\n");
	gets(opcion);
	if((strcmp(opcion,"N")==0)||(strcmp(opcion,"n")==0))
	{
	   return 1;
	}
	else
	{	
		/*Definimos la estrcutura para recibir los datos*/
	        struct stat InfArchivo;
	        int fd, EstadoOperacion, i;
		unsigned short Modo;
	        printf("Ingrese el nombre del archivo del cual desea analizar su información\n");
		gets(archivo);
	 	/*Imprimimos el nombre del archivo*/
		printf("Nombre del Archivo: %s\n",archivo);
	 	EstadoOperacion = stat(archivo, &InfArchivo);
		if(EstadoOperacion!=0)
		{
			printf("Error: %s Nombre de Archivo Incorrecto\n",archivo);
			return 1;
		}
		else
		{
		   /*Escribimos el identificador de usuario*/
		   printf("Identificador de usuario(uid): %d\n", InfArchivo.st_uid);
		   /*Escribimos el identificador de grupo*/
		   printf("Identificador de grupo (gid): %d\n", InfArchivo.st_gid);
	           /*Tamaño del archivo en bytes*/
	           printf("Tamaño del archivo: %lu bytes\n", InfArchivo.st_size);
	           /*Hora de Ultimo Acceso*/
 		   time_t tiempo; //variable para almacenar el tiempo
		   tiempo=InfArchivo.st_atime; //obetenemos el dato de acceso del i-node
		   struct tm *tiemporeal; //lo transformamos a un formato dd/mm/aa  hh/mm/ss
		   tiemporeal=localtime(&tiempo);
printf("Fecha y Hora de Ultimo Acceso: %02d/%02d/%02d  %02d:%02d:%02d\n",tiemporeal->tm_mday, tiemporeal->tm_mon, tiemporeal->tm_year+1900,tiemporeal->tm_hour,tiemporeal->tm_min,tiemporeal->tm_sec);
		   /*Hora de ultimo cambio*/
		   tiempo=InfArchivo.st_mtime;
		   tiemporeal=localtime(&tiempo);
printf("Fecha y Hora de Ultima Modificación: %02d/%02d/%02d %02d:%02d:%02d\n",tiemporeal->tm_mday,tiemporeal->tm_mon,tiemporeal->tm_year+1900,tiemporeal->tm_hour,tiemporeal->tm_min,tiemporeal->tm_sec);
		   /*Información de Protección/
		    /*Se cogen grupos de 3 bits del Modo de protección.
 * 		    los tres primeros son del dueño, los siguientes del grupo
 * 		    y los siguientes del mundo. Para estas operaciones se
 * 		    usan máscaras de bits y desplazamiento binario*/
		   printf("Información de Protección\n");
		   for(i=6;i>=0;i=i-3)
		   {
			Modo=InfArchivo.st_mode >> i;
			if(Modo & 04) printf ("r"); else printf("-");
			if(Modo & 02) printf ("w"); else printf("-");
			if(Modo & 01) printf ("x"); else printf("-");
		   }
		  printf("\n");
		 }
	  }//EndElse
	  printf("Pulse una tecla para continuar...\n");
	  gets(opcion);
	  return 1;
} 


